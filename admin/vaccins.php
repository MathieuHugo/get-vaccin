<?php

require('../inc/fonction.php');
require ('../inc/pdo2.php');
if (!isAdmin()) {
    header('Location: ../403.php');

}






$sql = "SELECT * FROM gv_vaccin
        WHERE suppression_vaccin = 'nodraft'
        ORDER BY nom_vaccin ASC";
$query = $pdo ->prepare($sql);
$query ->execute();
$vaccins = $query->fetchAll();


include ('inc/header.php');

?>



    <section id="page1">
        <div class="wrap3">
            <div class="table_vaccins">

                    <table style="width: 100%">
                        <colgroup>
                            <col class="hidden1" span="1" style="width: 20%;">
                            <col class="hidden2" span="1" style="width: 30%;">
                            <col class="hidden3" span="1" style="width: 10%;">
                            <col class="hidden4" span="1" style="width: 10%;">
                            <col class="hidden5" span="1" style="width: 10%;">
                            <col class="hidden6" span="1" style="width: 20%;">
                        </colgroup>
                        <thead>
                        <tr>
                            <th class="hidden2" colspan="6">
                                <h2 >Liste des vaccins</h2>
                                <a href="add_vaccin.php">
                                    <i class="fa-solid fa-square-plus"></i>
                                </a>
                            </th>
                        </tr>
                        <tr>
                            <th class="hidden1">Nom des vaccins</th>
                            <th class="hidden2">Maladie(s) ciblée(s)</th>
                            <th class="hidden3">Nombre de dose</th>
                            <th class="hidden4">Délai (en mois)</th>
                            <th class="hidden5">Status</th>
                            <th class="hidden6">Modifier/<br>Supprimer</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($vaccins as $vaccin) { ?>
                        <tr>
                            <td class="hidden1"><i class="fa-solid fa-syringe"></i> <?php echo $vaccin['nom_vaccin']?></td>
                            <td class="hidden2"><?php echo $vaccin['content']?></td>
                            <td class="hidden3"><?php echo $vaccin['nb_dose']?></td>
                            <td class="hidden4"><?php echo $vaccin['delai']?></td>
                            <td class="hidden5"><?php if($vaccin['status']=='actif') { echo ' <i class="fa-solid fa-circle" style="color: #18a40e; font-size: 0.8rem"></i> ' ;} else { echo ' <i class="fa-solid fa-circle" style="color: #b52a12; font-size: 0.8rem"></i>' ;}?> <?php echo $vaccin['status']?> </td>
                            <td class="hidden6" >
                                <div class="deuxicones">
                                    <a href="update_vaccin.php?id=<?php echo $vaccin['id']?>&&nom_vaccin=<?php echo $vaccin['nom_vaccin']?>&&content=<?php echo $vaccin['content']?>&&delai=<?php echo $vaccin['delai']?>&&status=<?php echo $vaccin['status']?>&&nb_dose=<?php echo $vaccin['nb_dose']?>"><i class="fa-solid fa-pencil"></i></a>
                                    <a href="delete_vaccin.php?id=<?php echo $vaccin['id']?>"><i class="fa-solid fa-trash"></i></a>
                                </div>
                            </td>
                        </tr>
                        <?php } ?>
                        </tbody>

                    </table>
            </div>
        </div>
    </section>












<?php
include ('inc/footer.php');