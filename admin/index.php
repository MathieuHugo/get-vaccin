<?php


require ('../inc/pdo2.php');
require ('../inc/fonction.php');


if (!isAdmin()) {
    header('Location: ../403.php');

}


$sql = "SELECT * FROM gv_contact
        ORDER BY created_at ASC";
$query = $pdo ->prepare($sql);
$query ->execute();
$commentaires = $query->fetchAll();




$page = $_SERVER['REQUEST_URI'];
$sql = "SELECT COUNT(*) AS total_rows FROM visiteurs";
$query = $pdo->prepare($sql);
$query->execute();
$result = $query->fetch();


$sql = "SELECT COUNT(*) AS total_rows FROM gv_user";
$query = $pdo->prepare($sql);
$query->execute();
$nbrusers = $query->fetch();

include ('inc/header.php');






?>


    <section id="page1">
        <div class="wrap3">
            <div class="blocall">
                <div class="bloc1et2">
                    <div class="bloc bloc1">

                        <p><span>Nombre de pages visitées <br><br></span><i class="fa-solid fa-eye">  </i><?php echo ' '.$result['total_rows']; ?></p>
                    </div>
                    <div class="bloc bloc2">
                        <p><span>Nombre d'utilisateurs inscrits<br><br></span> <i class="fa-solid fa-user"></i><?php echo ' '.$nbrusers['total_rows']; ?> </p>
                    </div>
                </div>
            </div>



<div class="table_commentaire">
    <table style="width: 100%">
        <colgroup class="hidden">
            <col span="1" style="width: 10%;">
            <col span="1" style="width: 25%;">
            <col span="1" style="width: 55%;">
            <col span="1" style="width: 10%;">
        </colgroup>
        <thead>
        <tr>
            <th class="hidden" colspan="4">Liste des commentaires</th>
        </tr>
        <tr>
            <th class="hidden">date</th>
            <th class="hidden">email</th>
            <th>message</th>
            <th>répondre</th>
        </tr>
        </thead>

        <tbody>
        <?php foreach ($commentaires as $commentaire) {?>
            <tr>
                <td class="hidden"><?php echo $commentaire['created_at']?></td>
                <td class="hidden"><?php echo $commentaire['email']?></td>
                <td style="!important;text-align: left"><?php echo $commentaire['message']?></td>
                <td><a href="answer_user.php?id=<?php echo $commentaire['id']?>"><i class="fa-solid fa-reply"></i></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>




        </div>
    </section>



<?php
include ('inc/footer.php');