<?php

require('../inc/pdo2.php');
require('../inc/fonction.php');

if (!isAdmin()) {
    header('Location: ../403.php');

}


$errors = [];
$roles = array (
        'actif' => 'actif',
        'non actif' => 'non actif'
);

$updatesuccess = false;


if (!empty($_POST['submitted'])) {
    // failles XXS
    $nom_vaccin = failleXSS('nom_vaccin');
    $content = failleXSS('content');
    $nb_dose=failleXSS('nb_dose');
    $delai = failleXSS('delai');
    $status = failleXSS('status');
    // validation
    $errors = validText($errors, 'nom_vaccin', $nom_vaccin, 1, 150);
    $errors = validText($errors,'content', $content,  1, 500);
    $errors = validNumber($errors, 'nb_dose', $nb_dose, 1, 500);
    $errors = validNumber($errors, 'delai', $delai, 1, 500);
    if(!empty($status)) {
        if(!array_key_exists($status, $roles )) {
            $errors['status'] = 'Error fucking hacker';
        }
    } else {
        $errors['status'] = 'Veuillez selectionner un status';
    }

    if(count($errors) == 0) {
        $sql = "INSERT INTO gv_vaccin (nom_vaccin, content,nb_dose, delai, status, suppression_vaccin)
        VALUES (:nom_vaccin, :content,:nb_dose, :delai, :status, 'nodraft')";
        $query = $pdo->prepare($sql);
        $query->bindValue('nom_vaccin', $nom_vaccin);
        $query->bindValue('content', $content);
        $query->bindValue('nb_dose', $nb_dose, PDO::PARAM_INT);
        $query->bindValue('delai', $delai, PDO::PARAM_INT);
        $query->bindValue('status', $status);
        $query->execute();
        $updatesuccess = true;
    }
}



include ('inc/header.php');

?>



    <section id="page1">
        <div class="wrap2">
            <?php if (!$updatesuccess) { ?>
            <div class="backgroundmodifform">
                <h2>Ajout d'un vaccin</h2>
                <form action="" method="post">
                    <div class="formbloc">
                        <label for="nom_vaccin">Nom du vaccin</label>
                        <input type="text" id="nom_vaccin" name="nom_vaccin">
                        <span class="errors"><?php viewError($errors, 'nom_vaccin'); ?></span>
                    </div>
                    <div class="formbloc">
                        <label for="content">Description</label>
                        <textarea name="content" id="content" cols="30" rows="10"></textarea>
                        <span class="errors"><?php viewError($errors, 'content'); ?></span>
                    </div>
                    <div class="formbloc">
                        <label for="nb_dose">Nombre de dose</label>
                        <input type="number" id="nb_dose" name="nb_dose" min="1" max="500">
                        <span class="error"><?php viewError($errors, 'nb_dose'); ?></span>
                    </div>
                    <div class="formbloc">
                        <label for="delai">Délai (en mois)</label>
                        <input type="number" id="delai" name="delai" min="1" max="500">
                        <span class="errors"><?php viewError($errors, 'delai'); ?></span>
                    </div>
                    <div class="formbloc">
                        <label for="status">status</label>
                        <select name="status" id="status">
                            <option value="" >__ selectionnez un status __</option>
                            <option value="actif" >actif</option>
                            <option value="non actif" >non actif</option>
                        </select>
                        <span class="errors"><?php viewError($errors, 'status'); ?></span>
                    </div>
                    <div class="formbloc">
                        <input type="submit" name="submitted" value="Ajouter">
                    </div>
                </form>
            </div>
            <?php } else { ?>
                <div class="backgroundmodifform2">
                    <h2>Ajout réussi !</h2>
                    <p>Le vaccin a été ajouté avec succès.</p>
                    <a href="vaccins.php"><div class="boutonretour">
                            Retour à la liste des vaccins
                        </div></a>
                </div>
            <?php } ?>
        </div>
    </section>












<?php
include ('inc/footer.php');