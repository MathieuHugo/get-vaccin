<?php
require('../inc/pdo2.php');
require('../inc/fonction.php');



if (!isAdmin()) {
    header('Location: ../403.php');

}

if (!empty($_GET['id'])) {
    $id = trim(strip_tags($_GET['id']));
    $sql = "UPDATE gv_vaccin
            SET suppression_vaccin = 'draft'
            WHERE id = :id";
    $query = $pdo->prepare($sql);
    $query ->bindValue('id', $id, PDO::PARAM_INT);
    $query->execute();
    header('Location: vaccins.php');
} else {
    die('404');
}
