<?php
require('../inc/pdo2.php');
require("../inc/fonction.php");

if (!isAdmin()) {
    header('Location: ../403.php');

}



$sql="SELECT * FROM gv_user
       WHERE suppression_user != 'delete'";
$query=$pdo->prepare($sql);
$query->execute();
$users=$query->fetchAll();





include ('inc/header.php');

?>


<section id="page1">
    <div class="wrap4">
        <div class="table_users">
        <table style="width: 100%">
            <colgroup>
                <col class="hidden7" style="width: 12%">
                <col class="hidden8" style="width: 10%">
                <col class="hidden9" style="width: 12%">
                <col class="hidden10" style="width: 12%">
                <col class="hidden11" style="width: 10%">
                <col class="hidden12" style="width: 16%">
                <col class="hidden13" style="width: 8%">
                <col class="hidden14" style="width: 8%">
                <col class="hidden15" style="width: 12%">
            </colgroup>
                <thead>
                    <tr class="hidden12">
                        <th  colspan="9"><h2>Liste d'utilisateurs</h2></th>
                    </tr>
                    <tr>
                        <th class="hidden7">Nom</th>
                        <th class="hidden8">Prénom</th>
                        <th class="hidden9">Date de Naissance</th>
                        <th class="hidden10">Email</th>
                        <th class="hidden11">Telephone</th>
                        <th class="hidden12" >Adresse</th>
                        <th class="hidden13">Rôle</th>
                        <th class="hidden14">Status</th>
                        <th class="hidden15">modifier/<br>supprimer</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($users as $user){ ?>
                    <tr>
                        <td class="hidden7"><?php echo $user['nom'] ?></td>
                        <td class="hidden8"><?php echo $user['prenom'] ?></td>
                        <td class="hidden9"><?php echo $user['naissance'] ?></td>
                        <td class="hidden10"><?php echo $user['email'] ?></td>
                        <td class="hidden11"><?php echo $user['telephone'] ?></td>
                        <td class="hidden12"><?php echo $user['adresse'].' '. $user['postale'].' '.$user['ville'] ?></td>
                        <td class="hidden13"><?php if($user['role']=='admin') { echo ' <i class="fa-solid fa-lock"></i> ' ;} else { echo ' <i class="fa-solid fa-user"></i> ' ;}?> <?php echo $user['role'] ?> </td>
                        <td class="hidden14"><?php if($user['active']=='actif') { echo ' <i class="fa-solid fa-circle" style="color: #18a40e; font-size: 0.8rem"></i> ' ;} else { echo ' <i class="fa-solid fa-circle" style="color: #b52a12; font-size: 0.8rem"></i>' ;}?> <?php echo $user['active'] ?></td>
                        <td colspan="2" class="deuxicones">
                            <a href="update_user.php?id=<?php echo $user['id']?>&&nom=<?php echo $user['nom']?>&&prenom=<?php echo $user['prenom']?>&&email=<?php echo $user['email']?>&&naissance=<?php echo $user['naissance']?>&&telephone=<?php echo $user['telephone']?>&&adresse=<?php echo $user['adresse']?>&&postale=<?php echo $user['postale']?>&&ville=<?php echo $user['ville']?>&&role=<?php echo $user['role']?>&&active=<?php echo $user['active']?>"><i class="fa-solid fa-pencil"></i></a>
                            <a href="delete_user.php?id=<?php echo $user['id']?>"><i class="fa-solid fa-trash"></i></a>

                        </td>
                    </tr>

                <?php } ?>
                </tbody>
            </table>
        </div>
        </div>
</section>




<?php
include ('inc/footer.php');