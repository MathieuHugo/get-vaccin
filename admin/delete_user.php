<?php
require('../inc/pdo2.php');
require('../inc/fonction.php');

if (!isAdmin()) {
    header('Location: ../403.php');

}


if (!empty($_GET['id'])) {
    $id = trim(strip_tags($_GET['id']));
    $sql = "UPDATE gv_user
            SET suppression_user = 'delete'
            WHERE id = :id";
    $query = $pdo->prepare($sql);
    $query ->bindValue('id', $id, PDO::PARAM_INT);
    $query->execute();
    header('Location: users.php');
} else {
    die('404');
}
