<?php

require('../inc/pdo2.php');
require('../inc/fonction.php');

if (!isAdmin()) {
    header('Location: ../403.php');

}



$errors = [];
$roles = array(
        'actif' => 'actif',
        'non actif' => 'non actif'
);

$updateSuccess = false;






$id = trim(strip_tags($_GET['id']));
$sql = "SELECT nom, prenom FROM gv_user
        WHERE id=:id";
$query = $pdo ->prepare($sql);
$query->bindValue('id', $id, PDO::PARAM_INT);
$query ->execute();
$user = $query ->fetch();


if (!empty($_POST['submitted'])) {
    // failles XXS
    $role = failleXSS('role');
    $active = failleXSS('active');

    if(!empty($active)) {
        if(!array_key_exists($active, $roles )) {
            $errors['active'] = 'Error fucking hacker';
        }
    } else {
        $errors['active'] = 'Veuillez selectionner un status';
    }


    if (empty($errors)) {
        $id = trim((strip_tags($_GET['id'])));
        $sql="UPDATE gv_user
                SET  role=:role, active=:active
                    WHERE id = :id";
        $query= $pdo->prepare($sql);
        $query->bindValue('role', $role,PDO::PARAM_STR);
        $query->bindValue('active', $active,PDO::PARAM_STR);
        $query->bindValue('id', $id,PDO::PARAM_INT);
        $query->execute();
        $updateSuccess = true;



    }
}


include ('inc/header.php');
?>



    <section id="page1">
        <div class="wrap2">
            <?php if (!$updateSuccess) { ?>
            <div class="backgroundmodifform">
                <h2>Modification de l'utilisateur :<br><?php echo $user['nom'].' '.$user['prenom']?></h2>
                    <form action="" method="post">
                        <div class="formbloc">
                            <label for="role">Rôle</label>
                            <select name="role" id="role">
                                <option value="user" <?php echo ($_GET['role'] == 'user') ? 'selected' : ''; ?>>User</option>
                                <option value="admin" <?php echo ($_GET['role'] == 'admin') ? 'selected' : ''; ?>>Admin</option>
                            </select>
                            <span class="errors"><?php viewError($errors, 'role'); ?></span>
                        </div>
                        <div class="formbloc">
                            <label for="status">Status</label>
                            <select name="active" id="status">
                                <option value="actif" <?php echo ($_GET['active'] == 'actif') ? 'selected' : ''; ?>>actif</option>
                                <option value="non actif" <?php echo ($_GET['active'] == 'non actif') ? 'selected' : ''; ?>>non actif</option>
                            </select>
                            <span class="errors"><?php viewError($errors, 'active'); ?></span>
                        </div>
                        <div class="formbloc">
                            <input type="submit" name="submitted" value="modifier">
                        </div>
                    </form>
            </div>
            <?php } else { ?>
                <div class="backgroundmodifform2">
                    <h2>Mise à jour réussie !</h2>
                    <p>L'utilisateur  a été modifié avec succès.</p>
                    <a href="users.php"><div class="boutonretour">
                            Retour à la liste des utilisateurs
                        </div></a>
                </div>
            <?php } ?>
        </div>
    </section>












<?php
include ('inc/footer.php');