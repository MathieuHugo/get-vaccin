<?php

require('../inc/pdo2.php');
require('../inc/fonction.php');

if (!isAdmin()) {
    header('Location: ../403.php');

}





$errors = [];
$updateSuccess = false;

$id = trim(strip_tags($_GET['id']));
$sql = "SELECT email FROM gv_contact
        WHERE id=:id";
$query = $pdo ->prepare($sql);
$query->bindValue('id', $id, PDO::PARAM_INT);
$query ->execute();
$content = $query ->fetch();
$email = trim(strip_tags($_SESSION['user']['email']));
//$subject = 'on verra';
if (!empty($_POST['submitted'])) {
    $reponse = failleXSS('reponse');
    $errors = validText($errors, 'reponse', $reponse, 1, 500);
    if (count($errors) == 0) {
//        $envoi = mail ($email, $subject, $reponse);
//        if ($envoi) {
//            echo "L'e-mail a été envoyé avec succès.";
//        } else {
//            echo "Erreur lors de l'envoi de l'email";
//        }
        $sql = "DELETE FROM gv_contact
                WHERE id=:id";
        $query = $pdo ->prepare($sql);
        $query->bindValue('id', $id, PDO::PARAM_INT);
        $query ->execute();
        $updateSuccess =true;

    }
}


include ('inc/header.php');
?>



    <section id="page1">
        <div class="wrap2">
            <?php if(!$updateSuccess) {?>
                <div class="backgroundmodifform">
                    <h2>Commentaire de : <?php echo $content['email']?></h2>
                    <form action="" method="post">
                        <div class="formbloc">
                            <label for="reponse">Votre réponse : </label>
                            <textarea name="reponse" id="reponse" cols="30" rows="10"></textarea>
                            <span class="errors"><?php viewError($errors, 'reponse'); ?></span>
                        </div>
                        <div class="formbloc">
                            <input type="submit" name="submitted" value="Envoyer">
                        </div>
                    </form>
                </div>
            <?php } else { ?>
                <div class="backgroundmodifform2">
                    <h2>Message envoyé !</h2>
                    <a href="index.php"><div class="boutonretour">
                            Retour à l'accueil
                        </div></a>
                </div>
            <?php } ?>
        </div>
    </section>












<?php
include ('inc/footer.php');