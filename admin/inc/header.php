<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin</title>
    <link rel="icon" href="../asset/img/logo2-removebg-preview.png" type="image/x-icon">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css" integrity="sha512-z3gLpd7yknf1YoNbCzqRKc4qyor8gaKU1qmn+CShxbuBusANI9QpRohGBreCFkKxLhei6S9CQXFEbbKuqLg0DA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Dancing+Script&family=Quicksand:wght@400;500;700&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="asset/css/style.css">
</head>
<header id="masthead">
    <div class="wrap">

        <div class="bloclogo">
            <div class="logo_img">
                <img src="../asset/img/logo2-removebg-preview.png" alt="logo accueil">
            </div>
            <h1>Get vaccin</h1>
            <div class="administrator"><p> <i class="fa-solid fa-lock"></i>&nbsp;&nbsp; ADMIN</p></div>

        </div>

        <div class="nav">
                <nav>
                    <ul>
                        <li><a href="../index.php" class="nav-link">
                                <i class="fa-solid fa-house"></i>
                                retour vers le site
                            </a>
                        </li>
                        <li> <a href="index.php" class="nav-link">
                                <i class="fa-solid fa-lock"></i>
                                Accueil
                            </a>
                        </li>
                        <li><a href="vaccins.php" class="nav-link">
                                <i class="fa-solid fa-syringe"></i>
                                liste des vaccins
                            </a></li>
                        <li><a href="users.php" class="nav-link">
                                <i class="fa-solid fa-user"></i>
                                liste des utilisateurs
                            </a>
                        </li>
                        <li>
                            <?php if(isLogged()){ ?>
                                <a href="../deconnexion.php" class="nav-link">
                                    <i class="fa-solid fa-power-off"></i>
                                    Deconnexion</a>
                            <?php } ?>
                        </li>
                    </ul>
                </nav>
        </div>
        <div class="dropdown">
                    <button><i class="fa-solid fa-bars"></i></button>
                    <div class="nav_burger">
                        <ul>
                            <li><a href="../index.php" class="nav-link">
                                    <i class="fa-solid fa-house"></i>
                                    retour vers le site
                                </a>
                            </li>
                            <li> <a href="index.php" class="nav-link">
                                    <i class="fa-solid fa-lock"></i>
                                    Accueil
                                </a>
                            </li>
                            <li><a href="vaccins.php" class="nav-link">
                                    <i class="fa-solid fa-syringe"></i>
                                    liste des vaccins
                                </a></li>
                            <li><a href="users.php" class="nav-link">
                                    <i class="fa-solid fa-user"></i>
                                    liste des utilisateurs
                                </a>
                            </li>
                            <li>
                                <?php if(isLogged()){ ?>
                                    <a href="../deconnexion.php" class="nav-link">
                                        <i class="fa-solid fa-power-off"></i>
                                        Deconnexion</a>
                                <?php } ?>
                            </li>
                        </ul>
                    </div>
        </div>




    </div>
</header>



<body>