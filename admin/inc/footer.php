<footer id="foot">
    <div class="wrap">
        <div class="titre">
            <h2>Get'Vaccin</h2>
        </div>
        <div class="menu_foot">
            <ul>
                <li><a href="#">Home</a></li>
                <li class="separtor">|</li>
                <li><a href="../contact.php">Contact</a></li>
                <li class="separtor">|</li>
                <li><a href="../mention.php">Mentions légales</a></li>
            </ul>
        </div>
        <div class="reseaux">
            <ul>
                <li><a href="https://www.instagram.com"><i class="fa-brands fa-instagram"></i></a></li>
                <li><a href="https://www.facebook.com"><i class="fa-brands fa-facebook"></i></a></li>
                <li><a href="https://www.twitter.com"><i class="fa-brands fa-x-twitter"></i></a></li>
                <li><a href="https://www.whatsapp.com"><i class="fa-brands fa-whatsapp"></i></a></li>
            </ul>
        </div>
        <div class="copyright">
            <p>copyright <i class="fa-regular fa-copyright"></i> 2023 inc Get'Vaccin</p>
        </div>
    </div>
</footer>
</body>
</html>