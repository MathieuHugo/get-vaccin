<?php



require("inc/fonction.php");
require ("inc/pdo2.php");

if (!isLogged()) {
    header('Location: ../403.php');
}

$sql="SELECT guv.id, guv.id_vaccin, guv.vaccin_at, gva.nom_vaccin, gva.content, gva.delai , gva.nb_dose, guv.nb_dose_effect
        FROM gv_user_vaccin AS guv
        LEFT JOIN gv_vaccin AS gva
        ON guv.id_vaccin = gva.id
        WHERE guv.id_user = :id
        AND gva.suppression_vaccin ='nodraft'";
$query= $pdo->prepare($sql);
$query->bindValue('id',$_SESSION['user']['id'],PDO::PARAM_INT);
$query->execute();
$gvu = $query->fetchAll();


include ("inc/header.php");

?>
    <section id="tableau_vaccination">
        <div class="wrap5">
            <h1>Carnet De Vaccination</h1>
            <div class="form_one">
                <table style="width: 100%">
                    <colgroup>
                        <col class="w1" span="1" style="width: 15%;">
                        <col class="w2" span="1" style="width: 10%;">
                        <col class="w3" span="1" style="width: 20%;">
                        <col class="w4" span="1" style="width: 15%;">
                        <col class="w5" span="1" style="width: 15%;">
                        <col class="w6" span="1" style="width: 20%;">
                    </colgroup>
                    <thead>
                    <tr>
                        <th class="hidden4">Date</th>
                        <th>Nom du vaccin</th>
                        <th class="hidden1">Maladie(s) ciblée(s) par le vaccin</th>
                        <th class="hidden2">Dose(s) effectuée(s)</th>
                        <th>Rappel</th>
                        <th class="hidden3">Modifier/ <br> Supprimer</th>
                    </tr>
                    </thead>
                    <?php foreach ($gvu as $gv) {?>
                        <tbody>
                            <tr>
                                <td class="hidden4"><?php echo date("d-m-Y", strtotime($gv['vaccin_at']))?></td>
                                <td><?php echo $gv['nom_vaccin']?></td>
                                <td class="hidden1"><?php echo $gv['content']?></td>
                                <td class="hidden2"><?php echo $gv['nb_dose_effect']?></td>
                                <?php if ($gv['nb_dose']>$gv['nb_dose_effect']){ ?>
                                    <td>
                                        <?php

                                        $datedujour = time();

                                        $delai=$gv['delai'] * 30 *3600* 24;

                                        $dateVaccins = strtotime($gv['vaccin_at']);


                                        if($datedujour - $dateVaccins >= $delai){
                                            echo 'Faites-vous vacciner !';
                                        }else{
                                            echo date('d/m/Y',$dateVaccins+$delai);
                                        }

                                        ?>
                                    </td>
                                <?php }else{ ?>
                                    <td>Nombres de doses nécessaires effectués !</td>
                                <?php } ?>

                                <td class="icon_d_m">
                                    <div class="img_icon">
                                        <a href="editvaccin.php?id=<?= $gv['id']; ?>&&vaccin_at=<?= $gv['vaccin_at']; ?>&&nom_vaccin=<?= $gv['nom_vaccin']; ?>&&nb_dose_effect=<?= $gv['nb_dose_effect'];?>"><img class="crayon" src="asset/img/editer.jpg" alt="editer"></a>
                                        <a href="delete_vaccin.php?id=<?php echo $gv['id']?>"><img src="asset/img/delete.jpg" alt="supprimer"></a>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    <?php } ?>
                </table>
            </div>
            <div class="button">
                <a href="addvaccin.php">Ajouter un vaccin</a>
            </div>
        </div>
    </section>
<?php
include ("inc/footer.php");