<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="asset/css/style.css">
    <title>404 Not Found</title>
</head>
<body>
<div class="container1">
    <div class="error-code">403</div>
    <div class="error-message">Page Not Found</div>
    <a href="index.php" class="back-home">Back to Home</a>
</div>
</body>
</html>