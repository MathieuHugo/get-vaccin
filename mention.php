<?php

require("inc/fonction.php");
require("inc/pdo2.php");


include ("inc/header.php"); ?>

    <section id="mention">
        <div class="wrap2">

            <h1>Mentions légales</h1>
            <p>Dernière mise à jour novembre 2023</p>

            <h2>Identification du site</h2>
            <p>Nom du site : Get'Vaccin<br>
                Adresse du site web : www.getvaccin.com<br>
                Adresse physique de l'entreprise : 439 Rue Piqure, Seringue-Ville, Paysland<br>
                Numéro de téléphone : +00 123 456 789<br>
                Adresse e-mail : contact@getvaccin.com</p>

            <h2>Éditeur du site</h2>
            <p>Raison sociale de l'entreprise : Get'Vaccin Ltd.<br>
                Forme juridique : Société par actions simplifiée<br>
                Capital social : 100 000 euros<br>
                Numéro d'inscription au registre du commerce : RC123456789<br>
                Adresse du siège social : 123 Rue Imaginaire, Villeville, Paysland</p>

            <h2>Directeur de la publication</h2>
            <p>Nom du responsable du contenu du site : Dr. A. Imaginaire<br>
                Coordonnées du responsable : 123 Rue Imaginaire, Villeville, Paysland |
                contact@getvaccin.com | +00 123 456 789</p>

            <h2>Hébergeur</h2>
            <p>Nom de la société d'hébergement : SecureHost Ltd.<br>
                Adresse de l'hébergeur : 456 Avenue Sécurité, Sécuritéville, Paysprotégé<br>
                Numéro de téléphone de l'hébergeur : +00 987 654 321</p>

            <h2>Finalité du site</h2>
            <p>Get'Vaccin a pour objectif de fournir un service en ligne permettant aux utilisateurs de gérer
                et de consulter leur carnet de vaccination de manière sécurisée et efficace.</p>

            <h2>Données de santé</h2>
            <p>Les informations liées à la santé collectées sur Get'Vaccin sont traitées
                de manière confidentielle et sécurisée, conformément aux réglementations en vigueur.</p>

            <h2>Consentement explicite</h2>
            <p>En utilisant le service de Get'Vaccin, l'utilisateur donne son consentement
                explicite pour la collecte et le traitement de ses données de santé aux fins spécifiées.</p>

            <h2>Sécurité des données</h2>
            <p>Get'Vaccin met en place des mesures de sécurité robustes pour assurer la protection
                des données de santé de ses utilisateurs.</p>

            <h2>Durée de conservation des données</h2>
            <p>Les données de santé seront conservées pendant une période de [indiquer la durée],
                après laquelle elles seront traitées conformément à notre politique de confidentialité.</p>

            <h2>Droit d'accès et de rectification</h2>
            <p>Les utilisateurs ont le droit d'accéder à leurs données de santé
                et de les rectifier en cas d'inexactitude.</p>

            <h2>Responsabilité</h2>
            <p>Get'Vaccin ne peut garantir l'exactitude totale des informations contenues dans le carnet
                de vaccination et décline toute responsabilité quant à l'utilisation
                des informations par les utilisateurs.</p>

            <h2>Liens externes</h2>
            <p>Get'Vaccin décline toute responsabilité quant au contenu des sites externes
                liés à partir de son site.</p>

            <h2>Droit applicable et litiges</h2>
            <p>Les présentes mentions légales sont soumises au droit de France.
                Tout litige sera de la compétence exclusive des tribunaux du commerce.</p>

        </div>
    </section>

<?php
include ("inc/footer.php");