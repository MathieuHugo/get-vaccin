<?php
require('inc/fonction.php');
require('inc/pdo2.php');

$success=false;
$errors = array();

if (!isLogged()) {
    header('Location: ../403.php');
    die();
}

$sql = "SELECT * FROM gv_vaccin
        WHERE suppression_vaccin = 'nodraft'
        ORDER BY nom_vaccin ASC";
$query = $pdo ->prepare($sql);
$query ->execute();
$vaccinall = $query->fetchAll();


if (!empty($_POST['submitted'])) {
    // FailleXSS
    $nomvaccin = failleXSS('nomvaccin');
    $date = trim(strip_tags($_POST['date']));
    $nb_dose_effect = failleXSS('nb_dose_effect');
    $id = $_GET['id'];

    $sqlSelectId = "SELECT id FROM gv_vaccin WHERE nom_vaccin = :nomvaccin";
    $querySelectId = $pdo->prepare($sqlSelectId);
    $querySelectId->bindValue('nomvaccin', $nomvaccin, PDO::PARAM_STR);
    $querySelectId->execute();

    if (empty($nb_dose_effect)) {
        $errors['nb_dose_effect'] = 'Veuillez rentrer le nombre de dose effectuer !';
    }
    if (empty($date)) {
        $errors['date'] = 'Veuillez rentrer une date !';
    }

    if (empty($errors)) {


        if ($row = $querySelectId->fetch(PDO::FETCH_ASSOC)) {
            $id_vaccin = $row['id'];

            $success = true;


            $sqlInsertUserVaccin = "UPDATE gv_user_vaccin 
                                SET id_vaccin = :id_vaccin, vaccin_at =:date, nb_dose_effect=:nb_dose_effect
                                WHERE id = :id";
            $queryInsertUserVaccin = $pdo->prepare($sqlInsertUserVaccin);
            $queryInsertUserVaccin->bindValue(':id_vaccin', $id_vaccin, PDO::PARAM_INT);
            $queryInsertUserVaccin->bindValue(':nb_dose_effect', $nb_dose_effect, PDO::PARAM_INT);
            $queryInsertUserVaccin->bindValue(':date', $date, PDO::PARAM_STR);
            $queryInsertUserVaccin->bindValue(':id', $id, PDO::PARAM_STR);
            $queryInsertUserVaccin->execute();


            $sqlSelectVaccin = "SELECT delai FROM gv_vaccin WHERE id = :id_vaccin";
            $querySelectVaccin = $pdo->prepare($sqlSelectVaccin);
            $querySelectVaccin->bindValue(':id_vaccin', $id_vaccin, PDO::PARAM_INT);
            $querySelectVaccin->execute();
            $vaccinInfo = $querySelectVaccin->fetch(PDO::FETCH_ASSOC);


            $delai = $vaccinInfo['delai'] * 30 * 3600 * 24;
            $dateVaccinTimestamp = strtotime($date);
            $dateRappelTimestamp = $dateVaccinTimestamp + $delai;
            $dateRappel = date('Y/m/d', $dateRappelTimestamp);


            $sqlUpdateUserVaccin = "UPDATE gv_user_vaccin SET rappel_at = :dateRappel WHERE id = :id";
            $queryUpdateUserVaccin = $pdo->prepare($sqlUpdateUserVaccin);
            $queryUpdateUserVaccin->bindValue(':dateRappel', $dateRappel, PDO::PARAM_STR);
            $queryUpdateUserVaccin->bindValue(':id', $id, PDO::PARAM_STR);
            $queryUpdateUserVaccin->execute();


        } else {

            $errors['nomvaccin'] = "Le nom du vaccin n'a pas été trouvé.";
        }

    }
}




include("inc/header.php");
?>

<section id="addvaccin">
    <div class="formuadd">
        <?php if(!$success){ ?>
        <div class="text_a">
            <h2>Modifier un vaccin</h2>
        </div>
        <form action="" method="post" novalidate>
            <input type="date" placeholder="Date" name="date" id="date" value="<?php if (empty($_POST)) { echo $_GET['vaccin_at']; }?>">
            <span class="error"><?php viewError($errors, 'date'); ?></span>

            <input type="number" placeholder="Nombre de dose deja effectuée" name="nb_dose_effect" id="nb_dose_effect" value="<?php if (empty($_POST)) { echo $_GET['nb_dose_effect']; }?>">
            <span class="error"><?php viewError($errors,'nb_dose_effect'); ?></span>

            <select name="nomvaccin" id="nomvaccin">
                <option value="">Sélectionnez un vaccin</option>
                <?php foreach ($vaccinall as $vaccina) { ?>
                    <option value="<?php echo $vaccina['nom_vaccin'] ?>"
                        <?php
                        // Vérifier si le nom_vaccin dans le GET correspond à la valeur actuelle
                        if (isset($_GET['nom_vaccin']) && $_GET['nom_vaccin'] === $vaccina['nom_vaccin']) {
                            echo ' selected';
                        }
                        ?>>
                        <?php echo $vaccina['nom_vaccin']; ?>
                    </option>
                <?php } ?>
            </select>
            <span class="error"><?php viewError($errors, 'nomvaccin'); ?></span>

            <input type="submit" name="submitted" value="Modifier un vaccin">
        </form>
    <?php }else{ ?>
            <div class="backgroundmodifform3">
                <h2>Vaccination modifié !</h2>
                <p>Retour au carnet des vaccins ?</p>
                <a href="carnet.php"><button> Carnet
                    </button></a>
            </div>
    <?php } ?>
    </div>
</section>

<?php
include("inc/footer.php");
?>