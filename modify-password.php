<?php
require("inc/fonction.php");
require("inc/pdo2.php");

$errors=array();
$updateSuccess=false;

if(!empty($_POST['submitted'])){
    $password=failleXSS('password');
    $password2=failleXSS('password2');

    if(!empty($password)&& !empty($password2)){
        if($password!=$password2){
            $errors['password']='Vos mot de passe sont différents';
        }elseif (mb_strlen($password) < 6){
            $errors['password']='Veuillez renseigner un mot de passe plus long de plus de 6 caractères ';
        }
    }else{
        $errors['password']= 'Veuillez renseigner les mots de passe';
    }

    if(count($errors)==0){
        $updateSuccess=true;
        $hashPassword= password_hash($password,PASSWORD_DEFAULT);
        $sql="UPDATE gv_user SET password=:password WHERE email=:mail";
        $query=$pdo->prepare($sql);
        $query->bindValue('password', $hashPassword,PDO::PARAM_STR);
        $query->bindValue('mail', $_GET['mail'],PDO::PARAM_STR );
        $query->execute();
        $query ->fetchAll();
    }
}

include ("inc/header.php");
?>
<section id="modifpass">
    <div class="wrap4">
        <?php if(!$updateSuccess){ ?>
        <h2>Changez votre mot de passe <i class="fa-regular fa-face-smile-wink"></i></h2>
        <form action=""method="post" novalidate>
            <input type="password" name="password" id="password" placeholder=" Nouveau mot de passe" value=<?php getPostValue('password');?>>
            <span class="error"><?php viewError($errors,'password');?></span>

            <input type="password" name="password2" id="password2" placeholder="Confirmez votre nouveau mot de passe" value=<?php getPostValue('password2');?>>

            <input type="submit" name="submitted" value="Confirmez">
        </form>
        <?php }else{ ?>
            <div class="wrap4">
                <div class="backgroundmodifform3">
                    <h2>Mot de passe modifié !</h2>
                    <p>Retour a la connexion ?</p>
                    <a href="connection.php"><button> Connexion
                        </button></a>
                </div>
            </div>
        <?php } ?>
    </div>
</section>


<?php
include("inc/footer.php");
