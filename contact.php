<?php
require ('inc/fonction.php');
require ('inc/pdo2.php');
$success = false;
$errors = [];


if(!isLogged()){
    $users="";
}else{
    $users=$_SESSION['user']['email'];
}

$updateSuccess = false;
if (!empty($_POST['submitted'])){
    //faille xss
    $mail = failleXSS('mail');
    $message = failleXSS('message');
    //validation
    $errors = validEmail($errors,$mail,'mail');
    $errors = validText($errors,'message',$message,5,500);
    if (count($errors) == 0){
        $updateSuccess = true;
        //insert into
        $sql = "INSERT INTO gv_contact (email,message,created_at)
                VALUES (:mail,:message,NOW())";
        $query = $pdo->prepare($sql);
        $query->bindValue('mail',$mail,PDO::PARAM_STR);
        $query->bindValue('message',$message,PDO::PARAM_STR);
        $query->execute();

    }
}

include ("inc/header.php");
?>

<section id="textarea">
     <div class="formulaire_contact">
<?php if (!$updateSuccess) { ?>
         <div class="text_c">
             <h1>Formulaire de contact</h1>
             <p>Une question ? Une problématique ? Contactez-nous.</p>
         </div>
        <div class="box">
            <form action="" method="post" novalidate>
                <input type="email" id="mail" name="mail" placeholder="Email"  value="<?php echo getPostValue('mail',$users); ?>">
                <span class="error"><?php echo viewError($errors, 'mail'); ?></span>

                <textarea id="message" name="message" placeholder="Message"><?php echo getPostValue('message'); ?></textarea>
                <span class="error"><?php echo viewError($errors,'message'); ?></span>

                <input type="submit" id="submit" name="submitted" value="Envoyer">
            </form>
        </div>
     </div>
<?php } else {  ?>
    <div class="wrap4">
        <div class="backgroundmodifform3">
            <h2>Message envoyé avec succès!</h2>
            <p>Retour a l'accueil</p>
            <a href="index.php"><button> Accueil
                </button></a>
        </div>
    </div>
<?php } ?>
</section>

<?php
include ("inc/footer.php");
