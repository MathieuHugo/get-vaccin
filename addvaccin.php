<?php
require('inc/fonction.php');
require('inc/pdo2.php');

$errors=array();
$updateSuccess = false;

if (!isLogged()) {
    header('Location: ../403.php');
}


$sql="SELECT * FROM gv_vaccin
      WHERE suppression_vaccin ='nodraft'";
$query=$pdo-> prepare($sql);
$query->execute();
$vaccins=$query->fetchAll();






if (!empty($_POST['submitted'])) {
    // FailleXSS
    $nomvaccin = failleXSS('nomvaccin');
    $id_user = trim(strip_tags($_SESSION['user']['id']));
    $date = trim(strip_tags($_POST['date']));
    $nb_dose_effect = failleXSS('nb_dose_effect');

    if (empty($date)) {
        $errors['date'] = 'Veuillez rentrer une date !';
    }

    if (empty($nb_dose_effect)) {
        $errors['nb_dose_effect'] = 'Veuillez rentrer le nombre de dose effectuer !';
    }

    $sql = "SELECT nb_dose FROM gv_vaccin WHERE nom_vaccin = :nomvaccin";
    $query = $pdo->prepare($sql);
    $query->bindValue('nomvaccin', $nomvaccin, PDO::PARAM_STR);
    $query->execute();
    $nb_dose = $query->fetchColumn();
    $errors = validNumber($errors, 'nb_dose_effect', $nb_dose_effect, 0, $nb_dose);

    if (empty($errors)) {
        $sqlSelectId = "SELECT id FROM gv_vaccin WHERE nom_vaccin = :nomvaccin";
        $querySelectId = $pdo->prepare($sqlSelectId);
        $querySelectId->bindValue('nomvaccin', $nomvaccin, PDO::PARAM_STR);
        $querySelectId->execute();
        $id_vaccin = $querySelectId->fetchColumn();

            $updateSuccess = true;

            $sqlInsertUserVaccin = "INSERT INTO gv_user_vaccin (id_user, id_vaccin, nb_dose_effect, vaccin_at, created_at) VALUES (:id_user, :id_vaccin, :nb_dose_effect, :date, NOW())";
            $queryInsertUserVaccin = $pdo->prepare($sqlInsertUserVaccin);
            $queryInsertUserVaccin->bindValue(':id_vaccin', $id_vaccin, PDO::PARAM_INT);
            $queryInsertUserVaccin->bindValue(':id_user', $id_user, PDO::PARAM_INT);
            $queryInsertUserVaccin->bindValue(':nb_dose_effect', $nb_dose_effect, PDO::PARAM_INT);
            $queryInsertUserVaccin->bindValue(':date', $date, PDO::PARAM_STR);
            $queryInsertUserVaccin->execute();
            $queryInsertUserVaccin->fetchAll();

            $id_last_inserted = $pdo->lastInsertId();
            $sqlSelectUserVaccin = "SELECT * FROM gv_user_vaccin WHERE id = :id_last_inserted";
            $querySelectUserVaccin = $pdo->prepare($sqlSelectUserVaccin);
            $querySelectUserVaccin->bindValue(':id_last_inserted', $id_last_inserted, PDO::PARAM_INT);
            $querySelectUserVaccin->execute();
            $userVaccinInfo = $querySelectUserVaccin->fetch(PDO::FETCH_ASSOC);
            $sqlSelectVaccin = "SELECT delai FROM gv_vaccin WHERE id = :id_vaccin";
            $querySelectVaccin = $pdo->prepare($sqlSelectVaccin);
            $querySelectVaccin->bindValue(':id_vaccin', $userVaccinInfo['id_vaccin'], PDO::PARAM_INT);
            $querySelectVaccin->execute();
            $vaccinInfo = $querySelectVaccin->fetch(PDO::FETCH_ASSOC);
            $delai = $vaccinInfo['delai'] * 30 * 3600 * 24; // Convertir le délai en secondes
            $dateVaccinTimestamp = strtotime($userVaccinInfo['vaccin_at']);
            $dateRappelTimestamp = $dateVaccinTimestamp + $delai;

            $dateRappel = date('Y/m/d', $dateRappelTimestamp);
            $sqlUpdateUserVaccin = "UPDATE gv_user_vaccin SET rappel_at = :dateRappel WHERE id = :id_last_inserted";
            $queryUpdateUserVaccin = $pdo->prepare($sqlUpdateUserVaccin);
            $queryUpdateUserVaccin->bindValue(':dateRappel', $dateRappel, PDO::PARAM_STR);
            $queryUpdateUserVaccin->bindValue(':id_last_inserted', $id_last_inserted, PDO::PARAM_INT);
            $queryUpdateUserVaccin->execute();


        } else {
            $errors['nomvaccin'] = "Le nom du vaccin n'a pas été trouvé.";
        }




}




include ("inc/header.php");
?>
    <section id="addvaccin">
        <div class="formuadd">
<?php if (!$updateSuccess) { ?>
            <div class="text_a">
                    <h2>Ajouter un vaccin</h2>
                </div>
                <form action="" method="post" novalidate>
                    <input type="date" placeholder="Date" name="date" id="date" value="<?php getPostValue('date'); ?>">
                    <span class="error"><?php viewError($errors,'date'); ?></span>

                    <select name="nomvaccin" id="nomvaccin">
                        <option value="">selectionnez un vaccin</option>

                        <?php foreach ($vaccins as $vacci) { ?>
                            <option value="<?php echo $vacci['nom_vaccin'] ?>"<?php
                            if(!empty($_POST['nomvaccin']) && $_POST['nomvaccin'] === $vacci['nom_vaccin']) {echo ' selected';}
                            ?>><?php echo $vacci['nom_vaccin']; ?></option>

                        <?php } ?>
                    </select>
                    <span class="error"><?php viewError($errors, 'nomvaccin'); ?></span>

                    <input type="number" placeholder="Nombre de dose deja effectuée" name="nb_dose_effect" id="nb_dose_effect" value="<?php getPostValue('nb_dose_effect'); ?>">
                    <span class="error"><?php viewError($errors,'nb_dose_effect'); ?></span>

                    <input type="submit" name="submitted" value="Ajouter un vaccin">

                </form>
<?php } else {  ?>
    <div class="wrap4">
        <div class="backgroundmodifform3">
            <h2>Vaccin Ajouté !</h2>
            <p>Retour au carnet des vaccins ?</p>
            <a href="carnet.php"><button> Carnet
                </button></a>
        </div>
    </div>

<?php } ?>
        </div>
    </section>

<?php



include ("inc/footer.php");