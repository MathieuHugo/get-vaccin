<?php
function debug($tableau) {
    echo '<pre style="height:200px;overflow-y: scroll;font-size: .5rem;padding: .6rem;font-family: Consolas, Monospace; background-color: #000;color: limegreen;">';
    print_r($tableau);
    echo '</pre>';
}
function failleXSS($id){
    return trim(strip_tags($_POST[$id]));
}
function getPostValue($key, $data='')
{
    if(!empty($_POST[$key])) {
        echo $_POST[$key];
    }elseif(!empty($data)) {
        echo $data;
    }
}

function viewError($err, $key)
{
    if(!empty($err[$key])) {
        echo $err[$key];
    }
}
function validText($err,$id,$var,$min,$max){
    if(!empty($var)){
        if(mb_strlen($var)<$min){
            $err[$id] = 'Veuillez renseigner plus de caractères !';
        }elseif(mb_strlen($var)>$max){
            $err[$id] = 'Veuillez renseigner moins de caractères !';
        }
    }else{
        $err[$id] = 'Veuillez renseigner ce champ !';
    }
    return $err;
}
function validNumber($err,$id,$var,$min,$max){
    if(!empty($var)&&is_numeric($var)){
        if(mb_strlen($var)<$min){
            $err[$id] = 'Veuillez renseigner plus de caractères !';
        }elseif(mb_strlen($var)>$max){
            $err[$id] = 'Veuillez renseigner moins de caractères !';
        }
    }else{
        $err[$id] = 'Veuillez renseigner ce champ !';
    }
    return $err;
}

function validPhoneNumber($err, $id, $var, $nbr) {
    if (!empty($var) && is_numeric($var)) {
        if (strlen($var) !== $nbr) {
            $err[$id] = 'Un numéro de téléphone est composé de 10 chiffres !';
        }
    } else {
        $err[$id] = 'Veuillez renseigner ce champ !';
    }
    return $err;
}




function validEmail($err,$var, $id){
    if(!empty($var)) {
        if (!filter_var($var, FILTER_VALIDATE_EMAIL)) {
            $err[$id] = 'Veuillez renseigner un email valide !';
        }
    } else {
        $err[$id] = 'Veuillez renseigner un email !';
    }
    return $err;
}

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[random_int(0, $charactersLength - 1)];
    }
    return $randomString;
}

function ValidVaccin($getVaccin,$err,$id) {
    if(empty($getVaccin)) {
        $err[$id] = 'Veuillez selectionner un vaccin';
    }
    return $err;
}


function isLogged()
{
    if(!empty($_SESSION['user']['id'])) {
        if(!empty($_SESSION['user']['email'])) {
            if(!empty($_SESSION['user']['role'])) {
                if(!empty($_SESSION['user']['ip'])) {
                    if($_SESSION['user']['ip'] == $_SERVER['REMOTE_ADDR']) {
                        return true;
                    }
                }
            }
        }
    }
    return false;
}

function isAdmin() {
    if(isLogged()) {
        if($_SESSION['user']['role'] == 'admin') {
            return true;
        }
    }
    return false;
}

function getAllArticles() {
    global $pdo;
    $sql = "SELECT * FROM gv_articles";
    $query = $pdo->prepare($sql);
    $query->execute();
    return $query->fetchAll();
}

function getInfoInput ($key) {
    if (!empty($_GET)) {
        echo $_GET[$key];
    }
}

function checkFutureDate($birthdate,$err, $key) {
    $currentDate = new DateTime();
    $birthDate = new DateTime($birthdate);

    if ($birthDate > $currentDate) {
        $err[$key] = 'La date de naissance ne peut pas être dans le futur.';
    }

    return $err;
}


function getUserId() {
    if (isset($_SESSION['user']['id'])) {
        return $_SESSION['user']['id'];
    } else {
        return null;
    }
}

function visiteurs()
{

    try {
        $pdo = new PDO('mysql:host=localhost;dbname=getvaccin', "root", "", array(
            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING
        ));
    } catch (PDOException $e) {
        echo 'Erreur de connexion : ' . $e->getMessage();
    }
    $page = $_SERVER['REQUEST_URI'];
    $sql = "INSERT INTO visiteurs (page, compte) VALUES (:page, 1)
        ON DUPLICATE KEY UPDATE compte = compte + 1";
    $query = $pdo->prepare($sql);
    $query->bindParam(':page', $page, PDO::PARAM_STR);
    $query->execute();

}