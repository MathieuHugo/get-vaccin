<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css" integrity="sha512-z3gLpd7yknf1YoNbCzqRKc4qyor8gaKU1qmn+CShxbuBusANI9QpRohGBreCFkKxLhei6S9CQXFEbbKuqLg0DA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Josefin+Sans:wght@300&family=Quicksand:wght@400;500;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="asset/css/style.css">
    <link rel="icon" href="asset/img/logo2-removebg-preview.png" type="image/x-icon">
    <title>Get'Vaccin</title>
</head>
<body>
<header id="masthead">
    <div class="wrap">
        <div class="logo">
            <a href="index.php">
            <div class="masthead_image">
                <img src="asset/img/logo2.jpg" alt="logo get'vaccin">
            </div>
            <h1>Get'Vaccin</h1>
            </a>
        </div>
        <nav>
            <ul>
                <li><a href="index.php"><i class="fa-solid fa-house"></i>Accueil</a></li>

                <?php if(!isLogged()){ ?>
                    <li class="carnet"><a href="#inscri-lien"><i class="fa-solid fa-syringe"></i>Carnet de vaccination</a></li>
                    <li><a href="inscription.php">Inscription</a></li>
                    <li><a href="connection.php">Connexion</a></li>
                <?php } else { ?>
                    <li class="carnet"><a href="carnet.php"><i class="fa-solid fa-syringe"></i>Carnet de vaccination</a></li>
                    <li class="profil_bouton"><a href="profil.php"><i class="fa-solid fa-user"> </i><?php echo $_SESSION['user']['surname'].' '.$_SESSION['user']['name']?></a></li>
                    <?php if(isAdmin()){ ?>
                        <li><a href="admin/index.php"><i class="fa-solid fa-lock"></i>Admin</a></li>
                    <?php } ?>
                    <li class="deconnexion"><a href="deconnexion.php"><i class="fa-solid fa-arrow-right-from-bracket"></i>Deconnexion</a></li>
                <?php } ?>
            </ul>
        </nav>
        <div class="dropdown">
            <button><i class="fa-solid fa-bars"></i></button>
            <div class="nav_burger">
                <ul>
                    <li><a href="index.php"><i class="fa-solid fa-house"></i>Accueil</a></li>
                    <li class="carnet"><a href="#inscri-lien"><i class="fa-solid fa-syringe"></i>Carnet de vaccination</a></li>
                    <?php if(!isLogged()){ ?>
                        <li><a href="inscription.php">Inscription</a></li>
                        <li><a href="connection.php">Connexion</a></li>
                    <?php } else { ?>
                        <li class="profil_bouton"><a href="profil.php"><i class="fa-solid fa-user"> </i><?php echo $_SESSION['user']['surname'].' '.$_SESSION['user']['name']?></a></li>
                        <?php if(isAdmin()){ ?>
                            <li><a href="admin/index.php"><i class="fa-solid fa-lock"></i>Admin</a></li>
                        <?php } ?>
                        <li><a href="deconnexion.php">Deconnexion</a></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>

</header>