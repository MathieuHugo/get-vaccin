<?php
require("inc/fonction.php");
require ("inc/pdo2.php");


if (!isLogged()) {
    header('Location: ../403.php');
}



$updateSuccess=false;
$errors=array();

$sql="SELECT * FROM gv_user WHERE id=:id";
$query=$pdo->prepare($sql);
$query->bindValue('id',$_SESSION['user']['id'],PDO::PARAM_STR);
$query->execute();
$user=$query->fetch();

if(!empty($_POST['submitted'])){
//    failleXSS
    $nom =failleXSS('nom');
    $prenom =failleXSS('prenom');
    $naissance =failleXSS('naissance');
    $mail =failleXSS('mail');
    $tel =failleXSS('tel');
    $adresse=failleXSS('adresse');
    $postale=failleXSS('postale');
    $ville=failleXSS('ville');


//    Validation des champs
    $errors = validText($errors,'nom', $nom,3,50);
    $errors = validText($errors,'prenom', $prenom,3,50);
    $errors = validEmail($errors,$mail,'mail');
    $errors = validPhoneNumber($errors,'tel',$tel,10);
    $errors =validText($errors, 'ville',$ville,3,150);
    $errors=  checkFutureDate($naissance, $errors, 'naissance');

    if (empty($errors['mail'])) {
        $sql = "SELECT id FROM gv_user
                        WHERE email = :mail";
        $query = $pdo ->prepare($sql);
        $query->bindValue('mail', $mail, PDO::PARAM_STR);
        $query->execute();

        $emailBDD = $query->fetch();
        if ($emailBDD && $emailBDD['id'] != $_SESSION['user']['id']) {
            $errors['mail'] = 'Mail déjà pris';

        }

    }

    if(count($errors)==0){
        $updateSuccess=true;
        $sql="UPDATE gv_user SET nom=:nom, prenom=:prenom, naissance=:naissance, email=:email, telephone=:tel, adresse=:adresse,postale=:postale,ville=:ville WHERE id=:id";

        $query=$pdo->prepare($sql);
        $query->bindValue('nom', $nom,PDO::PARAM_STR);
        $query->bindValue('prenom', $prenom,PDO::PARAM_STR);
        $query->bindValue('naissance', $naissance,PDO::PARAM_STR);
        $query->bindValue('email', $mail,PDO::PARAM_STR);
        $query->bindValue('tel', $tel,PDO::PARAM_STR);
        $query->bindValue('adresse', $adresse,PDO::PARAM_STR);
        $query->bindValue('postale', $postale,PDO::PARAM_INT);
        $query->bindValue('ville', $ville,PDO::PARAM_STR);
        $query->bindValue('id',$_SESSION['user']['id'],PDO::PARAM_STR);
        $query->execute();
    }
}
include ("inc/header.php");

?>

<section id="profil">
    <div class="wrap4">
        <?php if(!$updateSuccess){?>
        <div class="bloc">
            <h2>Modifiez votre profil !</h2>
            <form action="" method="post" novalidate>
                <label for="nom">Nom</label>
                <input type="text" name="nom" id="nom" value="<?php getPostValue('nom', $user['nom']); ?>">
                <span class="error"><?php viewError($errors,'nom'); ?></span>

                <label for="prenom">Prenom</label>
                <input type="text" name="prenom" id="prenom" value="<?php getPostValue('prenom', $user['prenom']); ?>">
                <span class="error"><?php viewError($errors,'prenom'); ?></span>

                <label for="age">Date de Naissance</label>
                <input type="date" name="naissance" id="age" value="<?php getPostValue('naissance', $user['naissance']); ?>">
                <span class="error"><?php viewError($errors,'naissance'); ?></span>

                <label for="mail">Email</label>
                <input type="email" name="mail" id="mail" value="<?php getPostValue('mail', $user['email']); ?>">
                <span class="error"><?php viewError($errors,'mail'); ?></span>

                <label for="tel">Téléphone</label>
                <input type="text" name="tel" id="tel" value="<?php getPostValue('tel', $user['telephone']); ?>">
                <span class="error"><?php viewError($errors,'tel'); ?></span>

                <label for="adresse">Adresse</label>
                <input type="text" name="adresse" id="adresse" value="<?php getPostValue('adresse', $user['adresse']); ?>">
                <span class="error"><?php viewError($errors,'adresse'); ?></span>

                <label for="postale">postale</label>
                <input type="text" name="postale" id="postale" value="<?php getPostValue('postale', $user['postale']); ?>">
                <span class="error"><?php viewError($errors,'postale'); ?></span>

                <label for="ville">Ville</label>
                <input type="text" name="ville" id="ville" value="<?php getPostValue('ville', $user['ville']); ?>">
                <span class="error"><?php viewError($errors,'ville'); ?></span>

                <input type="submit" name="submitted" value="Modifier">
            </form>
        </div>
        <?php }else{ ?>
            <div class="wrap4">
                <div class="backgroundmodifform3">
                    <h2>Profil modifié !</h2>
                    <p>Retour a la page d'acceuil ?</p>
                    <a href="index.php"><button>
                       Home </button></a>
                </div>
            </div>
        <?php } ?>
    </div>
</section>
<?php
include ("inc/footer.php");
