<?php




require("inc/fonction.php");
require("inc/pdo2.php");

if (isLogged()) {
    header('Location: ../403.php');

}


$errors=array();
$updateSuccess = false;

if(!empty($_POST['submitted'])){
//    FailleXSS
    $nom =failleXSS('nom');
    $prenom =failleXSS('prenom');
    $naissance = failleXSS('naissance');
    $mail =failleXSS('mail');
    $password =failleXSS('password');
    $password2 =failleXSS('password2');

//    Validation des champs
    $errors = validText($errors,'nom', $nom,3,50);
    $errors = validText($errors,'prenom', $prenom,3,50);
    $errors = validEmail($errors,$mail,'mail');
    $errors=  checkFutureDate($naissance, $errors, 'naissance');


    if(!empty($password)&& !empty($password2)){
        if($password!=$password2){
            $errors['password']='Vos mot de passe sont différents';
        }elseif (mb_strlen($password) < 6){
            $errors['password']='Veuillez renseigner un mot de passe plus long de plus de 6 caractères ';
        }
    }else{
        $errors['password']= 'Veuillez renseigner les mots de passe';
    }

    if (empty($errors['mail'])) {
        $sql = "SELECT id FROM gv_user
                        WHERE email = :mail";
        $query = $pdo ->prepare($sql);
        $query->bindValue('mail', $mail, PDO::PARAM_STR);
        $query->execute();
        $emailBDD = $query->fetch();
        if (!empty($emailBDD)) {
            $errors['mail'] = 'Mail déjà pris';

        }

    }

    if (empty($errors)) {
        $hashPassword= password_hash($password,PASSWORD_DEFAULT);
        $token =generateRandomString(130);
        $sql="INSERT INTO gv_user( nom, prenom, naissance, email, password, role, token, created__at, active, suppression_user) 
              VALUES (:nom,:prenom, :naissance,:email,:password,'user',:token,NOW(),'actif', 'no')";
        $query= $pdo->prepare($sql);
        $query->bindValue('nom', $nom,PDO::PARAM_STR);
        $query->bindValue('prenom', $prenom,PDO::PARAM_STR);
        $query->bindValue('naissance', $naissance,PDO::PARAM_STR);
        $query->bindValue('email', $mail,PDO::PARAM_STR);
        $query->bindValue('password', $hashPassword,PDO::PARAM_STR);
        $query->bindValue('token', $token,PDO::PARAM_STR);
        $query->execute();
        $inscr= $query->fetch();
        $updateSuccess = true;
    }

}


include ("inc/header.php");

?>
<section id="inscription">
    <div class="wrap2">
        <?php if (!$updateSuccess) { ?>
        <h2>Inscrivez-vous !</h2>
        <div class="contener">
            <div class="video">
                <div class="text_tuto">
                    <h3>Tutoriel</h3>
                    <p>Une video tutoriel en quelques étapes qui vous expliquera comment créer son carnet en quelques clics !</p>
                </div>
                <div class="video-item">
                    <iframe src="https://www.youtube.com/embed/IX8RgZ_ZAP4?si=dY8J1fOvKRxGLHn8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
                </div>
                <div class="imgborder">
                    <img src="asset/img/tel-main.jpg" alt="">

                    <img src="asset/img/calendar.jpg" alt="">

                    <img src="asset/img/accueil_3.jpg" alt="">
                </div>
            </div>
            <div class="formulaire">
                <form action="" method="post" novalidate>
                    <input type="text" name="nom" id="nom" placeholder="Nom" value="<?php getPostValue('nom'); ?>">
                    <span class="error"><?php viewError($errors,'nom'); ?></span>
                    
                    <input type="text" name="prenom" id="prenom" placeholder="Prenom" value="<?php getPostValue('prenom'); ?>">
                    <span class="error"><?php viewError($errors,'prenom'); ?></span>

                    <input type="date" name="naissance" id="naissance" placeholder="Date de naissance" value="<?php getPostValue('naissance'); ?>">
                    <span class="error"><?php viewError($errors,'naissance'); ?></span>
                    
                    <input type="email" name="mail" id="mail" placeholder="Email" value="<?php getPostValue('mail'); ?>">
                    <span class="error"><?php viewError($errors,'mail'); ?></span>
                    
                    <input type="password" name="password" id="password" placeholder="Mot de passe" value="<?php getPostValue('password'); ?>">
                    <span class="error"><?php viewError($errors,'password'); ?></span>
                    
                    <input type="password" name="password2" id="password2" placeholder="Confirmez votre mot de passe" value="<?php getPostValue('password2'); ?>">
                    <span class="error"><?php viewError($errors,'password2'); ?></span>
        
                    <input type="submit" name="submitted" value="S'inscrire">
                </form>
            </div>
        </div>

    </div>
    <?php } else {  ?>
        <div class="wrap4">
            <div class="backgroundmodifform2">
                <h6>Inscription réussie !</h6>
                <p>Vous êtes maintenant inscrit.</p>
                <a href="connection.php"><div class="boutonretour">
                        Connectez-vous
                    </div></a>
            </div>
        </div>

    <?php     } ?>
</section>


<?php


include ("inc/footer.php");
