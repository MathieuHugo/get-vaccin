<?php

require("inc/fonction.php");
require("inc/pdo2.php");

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'vendor/phpmailer/phpmailer/src/Exception.php';
require 'vendor/phpmailer/phpmailer/src/PHPMailer.php';
require 'vendor/phpmailer/phpmailer/src/SMTP.php';

$success=false;
$errors=array();

if (!empty($_POST['submitted'])) {
    // Faille XSS
    $mail = failleXSS('mail');

    $errors = validEmail($errors, $mail, 'mail');

    $sql = "SELECT email FROM gv_user";
    $query = $pdo->prepare($sql);
    $query->execute();
    $dbmail = $query->fetchAll();


    if (count($errors) == 0) {
        $success = true;

        $sql = "SELECT token FROM gv_user
        WHERE email = :mail";
        $query = $pdo->prepare($sql);
        $query->bindValue('mail', $mail, PDO::PARAM_STR);
        $query->execute();
        $token = $query->fetchColumn();
        $sendmail = new PHPMailer(true);

//        try {
//            // Paramètres du serveur SMTP
//            $sendmail->isSMTP();
//            $sendmail->Host = 'smtp-relay.brevo.com';
//            $sendmail->SMTPAuth = true;
//            $sendmail->Username = 'theodegremont2004@gmail.com';
//            $sendmail->Password = 'FlamantRose76';
//            $sendmail->SMTPSecure = 'ssl';
//            $sendmail->Port = 587;
//
//            // Destinataire et contenu
//            $sendmail->setFrom('papuchelechat@yahoo.com');
//            $sendmail->addAddress($mail);
//            $sendmail->isHTML(true);
//            $sendmail->Subject = 'Recuperation de Mot de Passe - GET\'Vaccin';
//            $sendmail->Body = "<div color: #114D67;> <h1> Cher(e) utilisateur, </h1> <br>
//            Nous avons bien reçu votre demande de réinitialisation de mot de passe. <br> Pour procéder, veuillez cliquer sur le lien ci-dessous :
//            http://localhost/php/Projet%20Vaccin/new-password.php?token={$token} <br>
//            <span style='font-weight: bold;'> Cordialement, <br>
//            Service de support technique </span> </div>";
//
//            $sendmail->AltBody = 'Contenu de l\'email en texte brut';
//
//
//            // Envoyer l'email
//            $sendmail->send();
//            echo 'L\'email a été envoyé avec succès.';
//        } catch (Exception $e) {
//            echo "Erreur lors de l'envoi de l'email : {$sendmail->ErrorInfo}";
//        }
    }
}


//if(!empty($_POST['submitted'])){
//    $mail=failleXSS('mail');
//    if(!empty($mail)){
//        $sql = "SELECT * FROM gv_user WHERE email=:mail";
//        $query = $pdo->prepare($sql);
//        $query->bindValue('mail', $mail, PDO::PARAM_STR);
//        $query->execute();
//        $user = $query->fetch();
//        debug($user);
//        if ($mail!=$user['email']){
//            $errors['mail'] = 'Cette email n\'existe pas !';
//        }else{
//            echo mail($mail,'Changez votre mot de passe !','<a href="modify-password.php?token='.urlencode($user['token']).'&mail='.urlencode($user['email']).'">Click ici pour modifier ton mot de passe</a>');
//        }
//    }else{
//        $errors['mail']='Veuillez renseigner un email !';
//    }
//}

include ("inc/header.php");
?>
<section id="form_mail">
    <?php if(!$success){ ?>
    <div class="wrap4">
        <h2>Mot de passe oublier ?</h2>
        <form action="" method="post" novalidate>
            <input type="email" name="mail" id="mail" placeholder="Votre email" value="<?php getPostValue('mail'); ?>">
            <span class="error"><?php viewError($errors,'mail'); ?></span>

            <input type="submit" name="submitted" value="Soumettre">
        </form>
    </div>
    <?php }else{?>
        <h2>Je sais que cette methode n'est pas sécurisée mais je ne pouvais pas envoyé de mail.</h2>
        <h2>C'est Antoine qui m'a dit de mettre ca <i class="fa-regular fa-face-smile"></i></h2>
        <p><a href="modify-password.php?token=<?php echo $token ?>&mail=<?php echo $mail ?>">Modifié votre mot de passe...</a></p>
    <?php } ?>
</section>


<?php
include('inc/footer.php');
