<?php
require("inc/fonction.php");
if (!isLogged()) {
    header('Location: ../403.php');

}


session_start();
$_SESSION = array();
session_destroy();
header('Location: index.php');
