<?php
require('inc/pdo2.php');
require('inc/fonction.php');



if (!isLogged()) {
    header('Location: ../403.php');

}

debug($_GET);


if (!empty($_GET['id'])) {
    $id = trim(strip_tags($_GET['id']));
    $sql = "DELETE FROM gv_user_vaccin
            WHERE id = :id";
    $query = $pdo->prepare($sql);
    $query ->bindValue('id', $id, PDO::PARAM_INT);
    $query->execute();
    header('Location: carnet.php');
} else {
    die('404');
}
