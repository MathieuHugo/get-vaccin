<?php
require("inc/fonction.php");
require("inc/pdo2.php");



visiteurs();

$sql = "SELECT COUNT(*) AS total_rows FROM gv_user";
$query = $pdo->prepare($sql);
$query->execute();
$nbrusers = $query->fetch();

$sql = "SELECT COUNT(*) AS total_rows FROM gv_user_vaccin";
$query = $pdo->prepare($sql);
$query->execute();
$nbrrappel = $query->fetch();

include ("inc/header.php");

?>



    <main>

        <section id="accueil">
            <div class="wrap">
                <div class="acc-title">
                    <h1><span class="a">Get'Vaccin, </span>votre santé, <span class="e">notre priorité.</span></h1>

                </div>
                <div class="acc-content">
                    <div class="acc-box">
                        <div class="acc-img">
                                <p>1.</p>
                            <img src="asset/img/accueil_1.jpg" alt="">

                        </div>
                        <div class="acc-text">
                            <p>Créez <span>rapidement</span> votre carnet de vaccin !</p>
                        </div>
                    </div>

                    <div class="acc-box">
                        <div class="acc-img">
                            <p>2.</p>
                            <img src="asset/img/acceuil_2.jpg" alt="">

                        </div>
                        <div class="acc-text">
                            <p>Un formulaire simple <span>à remplir</span>  en quelques minutes !</p>
                        </div>
                    </div>

                    <div class="acc-box">
                        <div class="acc-img">
                            <p>3.</p>
                            <img src="asset/img/accueil_3.jpg" alt="">
                        </div>
                        <div class="acc-text">
                            <p>Avec <span>un email</span> pour votre prochain <span>rappel</span>.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section id="inscri-lien">
            <div class="wrap2">
                <div class="insc-content">

                    <div class="incs-text">
                        <h2>Get'Vaccin</h2>
                        <p>Créer son carnet de vaccination en quelque clics !</p>
                        <div class="insc-button">
                        <?php if(isLogged()){?>
                        <a href="addvaccin.php" class="button">Commencer</a>
                        <?php }else{ ?>
                            <a href="inscription.php" class="button">Commencer</a>
                        <?php } ?>
                        </div>
                    </div>
                    <div class="incs-img">
                        <img src="asset/img/inscription_1.jpg" alt="">
                    </div>
                </div>
            </div>
        </section>

        <section id="article">
            <div class="wrap">
                <div class="art-content">
                    <a href="https://www.doctolib.fr/" target="_blank" class="art-lien">
                        <div class="art-text">
                            <h2>Doctolib</h2>
                            <p>Prenez votre rendez-vous ici !</p>
                        </div>
                        <img src="asset/img/lien_1.jpg" alt="" class="art-img">
                    </a>

                    <a href="https://www.santepubliquefrance.fr/" target="_blank" class="art-lien">
                        <div class="art-text">
                            <h2>Santé publique France</h2>
                            <p>Retrouvez ici les dernières actualités et travaux de Santé publique France !</p>
                        </div>
                        <img src="asset/img/lien_2.png" alt="" class="art-img">
                    </a>

                    <a href="https://www.ameli.fr/" target="_blank" class="art-lien">
                        <div class="art-text">
                            <h2>Ameli</h2>
                            <p>Vous n'êtes pas assuré ? Assurez-vous !</p>
                        </div>
                        <img src="asset/img/lien_3.jpg" alt="" class="art-img">
                    </a>
                </div>
            </div>
        </section>

        <section id="statistiques">
            <div class="wrap2">
                <div class="stat-content">
                    <p>Get'Vaccin c'est...</p>
                    <p><?php echo $nbrusers['total_rows']; ?><span> utilisateurs</span></p>
                    <p><?php echo $nbrrappel['total_rows']; ?><span> rappels</span></p>
                    <p>100% <span>d'avis positifs</span></p>
                </div>
            </div>
        </section>

    </main>

<?php
include ("inc/footer.php");
