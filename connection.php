<?php
require("inc/fonction.php");
require("inc/pdo2.php");


if (isLogged()) {
    header('Location: ../403.php');

}





$errors=array();
if(!empty($_POST['submitted'])){
//    failleXSS
    $mail =failleXSS('mail');
    $password =failleXSS('password');
    if(count($errors)==0){
        $sql="SELECT * FROM gv_user WHERE email=:mail;";
        $query = $pdo -> prepare($sql);
        $query->bindValue('mail', $mail ,PDO::PARAM_STR);
        $query->execute();
        $user = $query -> fetch();
        if(!empty($user)){

            if($user['active']=='actif') {
                if (password_verify($password, $user['password'])) {
                    $_SESSION['user'] = array(
                        'id' => $user['id'],
                        'name' => $user['nom'],
                        'surname' => $user['prenom'],
                        'email' => $user['email'],
                        'role' => $user['role'],
                        'ip' => $_SERVER['REMOTE_ADDR'],
                    );
                    header('Location: index.php');
                } else {
                    $errors['password'] = 'Votre mot de passe est incorrect !';
                }

            }else{
                $errors['mail'] = 'Votre compte est bloqué !';
            }
        }else{
            $errors['mail'] = 'Votre compte n\'existe pas !';
        }
    }

}



include ("inc/header.php");
?>

<section id="connect">
    <div class="wrap4">
        <div class="formulaire_connect">
            <h2>Connexion</h2>
            <form action="" method="post" novalidate>
                <input type="email" name="mail" id="mail" placeholder="Email" value="<?php getPostValue('email'); ?>">
                <span class="error"><?php viewError($errors,'mail'); ?></span>

                <input type="password" name="password" id="password" placeholder="Mot de passe" value="">
                <span class="error"><?php viewError($errors,'password'); ?></span>

                <input type="submit" name="submitted" value="Se connecter">
            </form>
            <p><a href="forget-password.php">Mot de passe oublié ?</a></p>
        </div>
    </div>
</section>









<?php
include ("inc/footer.php");
