
# GetVaccin

Un carnet de vaccin numerique avec rappel.


## Authors

- [@theodegremont,@gd.lotr.gf,@MathieuHugo](https://gitlab.com/test5805444/projet-1-carnet-de-vaccination.git)

## Color Reference

| Color             | Hex                                                                |
| ----------------- | ------------------------------------------------------------------ |
    --vert: #0cbd67;
    --bleufonce:#114D67;
    --bleuclair : #2C84DB;
    --blancbleu : #F6FEFF;
    --gris : #889EB8;


## Demo

Insert gif or link to demo



1. Clone Get'Vaccin repository:
```sh
git clone https://gitlab.com/test5805444/projet-1-carnet-de-vaccination.git
```
 
2. Change to the project directory:
```sh
cd projet-vaccination
```
 
3. Install the dependencies:
```sh
composer install
```
 
---
## Used By

This project is used by the following companies:

- Get'Vaccin.
- NeedForSchool.